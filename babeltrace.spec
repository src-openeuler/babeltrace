Name: babeltrace
Version: 1.5.11
Release: 1
Summary: Common Trace Format Babel Tower
License: MIT AND GPL-3.0-or-later WITH Bison-exception-2.2 AND LGPL-2.1-only AND BSD-4-Clause-UC
URL: https://www.efficios.com/babeltrace
Source0: https://www.efficios.com/files/%{name}/%{name}-%{version}.tar.bz2

BuildRequires: bison >= 2.4 flex >= 2.5.35 glib2-devel >= 2.22.0
BuildRequires: popt-devel >= 1.13 swig >= 2.0 elfutils-devel >= 0.154
BuildRequires: libuuid-devel python3-devel python3-setuptools libtool gcc perl-Test-Harness

Provides:      lib%{name}
Obsoletes:     lib%{name}

%description
Babeltrace is a trace format converter. It is able to read and write different trace
formats, such as the Common Trace Format (CTF). Babeltrace also acts as the CTF
reference implementation.

%package -n lib%{name}-devel
Summary: Common Trace Format Babel Tower
Requires: %{name} = %{version}-%{release} glib2-devel

%description -n lib%{name}-devel
Babeltrace is a trace format converter. It is able to read and write different trace
formats, such as the Common Trace Format (CTF). Babeltrace also acts as the CTF
reference implementation.

%package -n python3-%{name}
Summary: Common Trace Format Babel Tower
Requires: %{name} = %{version}-%{release}

%description -n python3-%{name}
Babeltrace is a trace format converter. It is able to read and write different trace
formats, such as the Common Trace Format (CTF). Babeltrace also acts as the CTF
reference implementation.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
export PYTHON=%{__python3}
export PYTHON_CONFIG=%{__python3}-config
%configure --disable-static --enable-python-bindings
%make_build

%install
rm -rf $RPM_BUILD_ROOT
%make_install
find %{buildroot} -type f -name "*.la" -delete -print
#Remove installed doc
rm %{buildroot}/%{_docdir}/%{name}/{LICENSE,API.txt,gpl-2.0.txt,mit-license.txt,std-ext-lib.txt}

%check
make check

%pre

%preun

%post  -n %{name} -p /sbin/ldconfig

%postun -n %{name} -p /sbin/ldconfig

%files
%doc ChangeLog doc/lttng-live.txt
%license LICENSE gpl-2.0.txt mit-license.txt
%{_bindir}/%{name}*
%{_libdir}/lib%{name}*.so.*

%files -n lib%{name}-devel
%{_includedir}/%{name}
%{_libdir}/lib%{name}*.so
%{_libdir}/pkgconfig/babeltrace.pc
%{_libdir}/pkgconfig/babeltrace-ctf.pc

%files -n python3-%{name}
%{python3_sitearch}/babeltrace
%{python3_sitearch}/babeltrace*.egg-info

%files help
%{_mandir}/man1/*.1.gz

%changelog
* Sun Feb 4 2024 hanhuihui <hanhuihui5@huawei.com>  - 1.5.11-1
- upgrade version to 1.5.11

* Wed Aug 03 2022 wangkerong <wangkerong@h-partners.com>  - 1.5.8-6
- revert remove rpath

* Fri Apr 22 2022 lin zhang <lin.zhang@turbolinux.com.cn>  - 1.5.8-5
- Fix install failed

* Sat Apr 16 2022 hanhui <hanhui15@h-partners.com> - 1.5.8-4
- Desc:python3 Mapping function change lead to build failed

* Sat Sep 4 2021 Hongxun Ren<renhongxun@huawei.com> - 1.5.8-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:remove rpath

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 1.5.8-2
- DESC: delete -Sgit from %autosetup, and delete BuildRequires git

* Thu Jul 16 2020 wenzhanli<wenzhanli2@huawei.com> - 1.5.8-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:Update version 1.5.8

* Fri Feb 28 2020 chengquan<chengquan3@huawei.com> - 1.5.6-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix bug in spell check

* Fri Sep 27 2019 chengquan<chengquan3@huawei.com> - 1.5.6-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add help package

* Tue Aug 13 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.5.6-2
- Package init

